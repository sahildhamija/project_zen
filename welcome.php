<?php 
    require_once 'controllers/authController.php';
?>
<!doctype html>
<html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="UTF-8" />
<title>Zendesk | Customer support ticket system &amp; sales CRM software company</title>

<script>
    dataLayer = [{
    "site_section": "homepage"
}];
  </script>
<link href="style.css" media="all" rel="stylesheet" type="text/css" />
</head>
<body data-useragent class="lang-en responsive " lang="en-US">
<script>
  function loadJS(u) {
    var r = document.getElementsByTagName("script")[0],
      s = document.createElement("script");
    s.src = u;
    r.parentNode.insertBefore(s, r);
  }

  if (!window.HTMLPictureElement || document.msElementsFromPoint) {
    loadJS('respimg.min.js');
  }
</script> <header class="global-header js-global-header transparent" role="banner">
<div class="container nav-container">
<nav class="secondary-nav show-small-up">
<ul class="menu SL_swap" id="smartling-secondary-nav">
<li><a class="secondary-link" href="index.html">Log out</a></li>
<li><a class="secondary-link" href="#">Product Support</a></li>
<li class="has-dropdown">
<a class="secondary-link open-dropdown js-open-dropdown" href="#">Company<span class="ent-text"></span></a>
<ul class="dropdown dropdown-secondary">
<li class="list-item">
<a class="block-link" href="#">About us</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Press</a>
</li>
<li class="list-item SL_norewrite">
<a class="block-link" href="#">Investors</a>
</li>
<li class="list-item SL_norewrite">
<a class="block-link" href="#">Events</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Careers</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Diversity & Inclusion</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Social Impact</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Legal</a>
</li>
</ul>
</li>
<li><a class="secondary-link" href="#">Contact us</a></li>
</ul>
<ul class="menu">
<li class="has-dropdown">
<div class="list list-vert secondary-menu-item language-menu">
<a class="secondary-link open-dropdown" href="index.html">English</a>
<ul class="dropdown dropdown-secondary">
<li class="list-item"><a class="sl_norewrite block-link" data-lang="en-us" href="index.html">English (US)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="en-gb" href="#">English (UK)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="es-es" href="#">Español</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="es-la" href="#">Español (LATAM)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="pt-br" href="#">Português</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="fr-fr" href="#">Français</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="de-de" href="#">Deutsch</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="it-it" href="#">Italiano</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="nl" href="#">Nederlands</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="ja-jp" href="#">日本語</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="ru-ru" href="#">Pусский</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="ko-kr" href="#">한국어</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="zh-tw" href="#">繁體中文 (台灣)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="zh-tw" href="#">繁體中文 (香港特區)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="th-th" href="#">ไทย</a></li> </ul>
</div>
</li>
</ul>
</nav>
<div class="sticky-marker js-sticky-marker"></div>
<div class="sticky-container js-sticky-container">
<nav class="primary-nav">
<ul class="menu logo-items">
<li class="logo">
<a href="index.html" class="" alt="Logo here"></a>
</li>
<li class="toggle-mobile-menu show-mobile-only js-toggle-mobile-menu js-hide-body-scroll">
<a href="#">Toggle Mobile Menu</a>
</li>
</ul>
<ul class="menu nav-items">
<li class="has-dropdown">
<span class="open-dropdown js-open-dropdown primary-link">Products</span>
<div class="dropdown dropdown-primary dropdown-wide">
<div class="dropdown-inner">
<div class="columns SL_swap" id="global-header-products-dropdown">
<div class="column column-8 products">
<div class="intro">
<span class="subhead">PRODUCTS</span>
</div>
<div class="columns product-list">
<ul class="column">
<li>
<a href="#" class="block-link support">
<div class="copy">
<p class="h3">Support</p>
<p>Integrated customer support</p>
</div>
<div class="icon product-icon support"></div>
</a>
</li>
<li>
<a href="#" class="block-link guide">
<div class="copy">
<p class="h3">Guide</p>
<p>Knowledge base and smart self-service</p>
</div>
<div class="icon product-icon guide"></div>
</a>
</li>
<li>
<a href="#" class="block-link chat">
<div class="copy">
<p class="h3">Chat</p>
<p>Live chat and messaging</p>
</div>
<div class="icon product-icon chat"></div>
</a>
</li>
<li>
<a href="#" class="block-link talk">
<div class="copy">
<p class="h3">Talk</p>
<p>Call center software</p>
</div>
<div class="icon product-icon talk"></div>
</a>
</li>
</ul>
<ul class="column">
<li>
<a href="#" class="block-link sell">
<div class="copy">
<p class="h3">Sell</p>
<p>Sales CRM</p>
</div>
<div class="icon product-icon sell"></div>
</a>
</li>
<li>
<a href="#" class="block-link explore">
<div class="copy">
<p class="h3">Explore</p>
<p>Analytics and reporting</p>
</div>
<div class="icon product-icon explore"></div>
</a>
</li>
<li>
<a href="#" class="block-link gather">
<div class="copy">
<p class="h3">Gather</p>
<p>Community forum</p>
</div>
<div class="icon product-icon gather"></div>
</a>
</li>
<li>
<a href="#" class="block-link connect">
<div class="copy">
<p class="h3">Connect</p>
<p>Proactive campaigns</p>
</div>
<div class="icon product-icon connect"></div>
</a>
</li>
</ul>
</div>
<div class="intro">
<span class="subhead">BUNDLES</span>
</div>
<div class="columns product-list">
<div class="column">
<a href="#" class="block-link">
<div class="copy">
<p class="h3">The Zendesk Suite</p>
<p>Four products, one package: Support, Guide, Chat, and Talk</p>
</div>
</a>
</div>
<div class="column">
<a href="#" class="block-link">
<div class="copy">
<p class="h3">Duet</p>
<p>Duet combines the power of Sell and Support in a single super seat</p>
 </div>
</a>
</div>
</div>
</div>
<div class="column column-4 accent">
<div class="intro">
<span class="subhead">Platform</span>
</div>
<ul>
<li>
<a href="#" class="block-link">
<div class="copy">
<p class="h3">Zendesk Sunshine</p>
<p>The Zendesk platform: open, flexible, and powerful enough to build the best customer experiences</p>
</div>
</a>
</li>
<li>

<a href="#" class="block-link">
<div class="copy">
<p class="h3">Sunshine Conversations</p>
<p>A new part of Sunshine that lets you build interactive messaging experiences</p>
</div>
</a>
</li>
<li>
<a href="#" class="block-link">
<div class="copy">
<p class="h3">Marketplace</p>
<p>Browse apps, integrations, and partners</p>
</div>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</li>
<li>
<a class="primary-link" href="pricing.html">Pricing</a>
</li>
<li class="has-dropdown">
<span class="open-dropdown js-open-dropdown primary-link">Solutions</span>
<div class="dropdown dropdown-primary dropdown-wide solutions-columns">
<div class="dropdown-inner">
<div class="columns">
<div class="column column-6">
<div class="intro">
<span class="subhead">INDUSTRY</span>
</div>
<div class="columns">
<ul class="column-6">
<li>
<a class="block-link slim" href="#">
<span class="h3">Financial services</span>
</a>
</li>
<li class="industry-solutions-government">
<a class="block-link slim" href="#">
<span class="h3">Government</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">Healthcare</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">Manufacturing</span>
</a>
</li>
</ul>
<ul class="column-6">
<li>
<a class="block-link slim" href="#">
<span class="h3">Media</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">Retail</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">Telecommunications</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">Software</span>
</a>
</li>
</ul>
</div>
</div>
<div class="column column-3">
<div class="intro">
<span class="subhead">COMPANY TYPE</span>
</div>
<ul>
<li>
<a class="block-link slim" href="#">
<span class="h3" data-sl-variant="global-header-enterprise">Enterprise</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">SMB</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">Startups</span>
</a>
</li>
</ul>
</div>
<div class="column column-3">
<div class="intro">
<span class="subhead">USE-CASE</span>
</div>
<ul>
<li>
<a class="block-link slim" href="#">
<span class="h3">Self-service</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">Omnichannel</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">Internal help desk</span>
</a>
</li>
<li>
<a class="block-link slim" href="#">
<span class="h3">Sales CRM</span>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</li>
<li>
<a class="primary-link" href="#">Demo</a>
</li>
<li>
<a class="primary-link" href="#">Services</a>
</li>
<li class="has-dropdown">
<a class="open-dropdown js-open-dropdown primary-link" href="#">Resources</a>
<div class="dropdown dropdown-primary dropdown-wide">
<div class="dropdown-inner">
<div class="intro hide-mobile-only">
<p class="subhead">Resources</p>
</div>
<ul class="inline-list col-3 SL_swap" id="global-header-resources-dropdown">
<li>
<a class="block-link" href="#">
<p class="h3">Library</p>
<p>Guides, research, videos, and resources</p>
</a>
</li>
<li>
<a class="block-link" href="#">
<p class="h3">Events</p>
<p>Come meet us in person</p>
</a>
</li>
<li>
<a class="block-link" href="#">
<p class="h3">Partners</p>
<p>How to locate or become a Zendesk partner</p>
</a>
</li>
<li>
<a class="block-link" href="#">
<p class="h3">Blog</p>
<p>News, tips, and best practices</p>
</a>
</li>
<li>
<a class="block-link" href="#">
<p class="h3">Live webinars</p>
<p>Online events</p>
</a>
</li>
<li>
<a class="block-link" href="#">
<p class="h3">API & Developers</p>
<p>Info for building things with Zendesk</p>
</a>
</li>
<li>
<a class="block-link" href="#">
<p class="h3">Customer stories</p>
<p>See what success with Zendesk looks like</p>
</a>
</li>
<li>
<a class="block-link" href="#">
<p class="h3">Zendesk Relate</p>
<p>Our global user conference. And our digital magazine.</p>
</a>
</li>
<li class="hidden-ja-important">
<a class="block-link" href="#">
<p class="h3">Repeat Customer</p>
<p>A podcast about amazing customer experiences</p>
</a>
</li>
</ul>
</div>
</div>
</li>

<li class="has-dropdown mobile-secondary-nav hide-small-up">
<a class="open-dropdown js-open-dropdown primary-link" href="#">Company</a>
<div class="dropdown dropdown-primary">
<div class="dropdown-inner">
<ul>
<li><a class="block-link" href="#">Contact us</a></li>
<li class="list-item">
<a class="block-link" href="#">About us</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Press</a>
</li>
<li class="list-item SL_norewrite">
<a class="block-link" href="#">Investors</a>
</li>
<li class="list-item SL_norewrite">
<a class="block-link" href="#">Events</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Careers</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Diversity & Inclusion</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Social Impact</a>
</li>
<li class="list-item">
<a class="block-link" href="#">Legal</a>
</li>
</ul>
</div>
</div>
</li>
<li class="has-dropdown mobile-secondary-nav hide-small-up">
<span class="open-dropdown js-open-dropdown primary-link">Language</span>
<div class="dropdown dropdown-primary">
<div class="dropdown-inner">
<ul>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="en-us" href="#">English (US)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="en-gb" href="#">English (UK)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="es-es" href="#">Español</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="es-la" href="#">Español (LATAM)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="pt-br" href="#">Português</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="fr-fr" href="#">Français</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="de-de" href="#">Deutsch</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="it-it" href="#">Italiano</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="nl" href="#">Nederlands</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="ja-jp" href="#">日本語</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="ru-ru" href="#">Pусский</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="ko-kr" href="#">한국어</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="zh-tw" href="#">繁體中文 (台灣)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="zh-tw" href="#">繁體中文 (香港特區)</a></li>
<li class="list-item"><a class="sl_norewrite block-link" data-lang="th-th" href="#">ไทย</a></li> </ul>
</div>
</div>
</li>
<div class="nav-separator hide-small-up"></div>
<li class="has-cta">
<a class="cta primary small" href="index.html">Welcome, <?php echo $_SESSION['username']; ?></a>
</li>
<li class="has-cta hide-small-up"><a class="primary-link" href="#">Log in</a></li>
</ul>
</nav>
</div>
</div> 
</header>
<article class="p-home js-activate-page-section" data-page-section-trigger="60">
<section class="hide floating-promo js-promo-top js-floating-promo SL_swap" id="homepage-top-promo" data-location="top" data-cookie-name="home-top-promo-20200130">
<div class="container">

<div class="active-promo"> 
<div class="container flex-content">
<p class="js-promo-body-title">CX on the Beach: Join us in Miami at Zendesk's Global User Conference, March 3-5 2020.</p>
<a class="cta tertiary" href="#">Register now.</a>
</div>
</div>
</div>
<a href="#" class="close js-close" aria-label="Close promo">&times;</a>
</section>
<section class="hero js-hero">
<div class="container grid">
<div class="row row-hero">
<div class="col col-small-7 hero-copy-container">
<h1 class="hero-heading strong" data-personalize="hero-heading">Build the best customer experiences</h1>
<h5 class="hero-copy" data-personalize="hero-copy">Our CRM software for support, sales, and customer engagement is designed to create better customer relationships.</h5>
<div class="cta-section cta-section-hero">
<a href="#" class="button button-large button-primary-default" target="" rel="">Free trial</a> <a href="#" class="button button-large button-secondary-default" target="" rel="">View demo</a> </div>
</div>
<div class="hero-image-container js-hero-image-container" data-randomize="true" data-personalize="hero-images">
<img class="lazyload no-src hero-image js-hero-image js-localized-content js-localized-content-no-show" data-hide-for="en-us en-gb es-es es-mx pt-br fr-fr de-de nl ru-ru zh-tw" data-bg-color="#F9CCBD" src="#" alt="Portrait photo">
<img class="lazyload no-src hero-image js-hero-image js-localized-content js-localized-content-no-show" data-hide-for="en-gb ru-ru ko-kr th-th it-it zh-tw" data-bg-color="#F9CCBD" src="#" alt="Portrait photo">
 <img class="lazyload no-src hero-image js-hero-image js-localized-content js-localized-content-no-show" data-hide-for="ko-kr th-th it-it" data-bg-color="#EDE1CF" src="#" alt="Portrait photo">
<img class="lazyload no-src hero-image js-hero-image js-localized-content js-localized-content-no-show" data-hide-for="ru-ru ko-kr th-th it-it zh-tw" data-bg-color="#D6EEF2" src="#" alt="Portrait photo">
<img class="lazyload no-src hero-image js-hero-image js-localized-content js-localized-content-no-show" data-hide-for="ru-ru ko-kr th-th it-it zh-tw" data-bg-color="#FFD7B3" src="#" alt="Portrait photo">
<img class="lazyload no-src hero-image js-hero-image js-localized-content js-localized-content-no-show" data-hide-for="ko-kr th-th it-it zh-tw" data-bg-color="#F3F0EE" src="#" alt="Portrait photo">
</div>
</div>
</div>
</section>
<section class="section-use-cases js-section-use-cases">
<div class="container grid centered">
<div class="row row-use-cases-heading js-use-cases-heading js-localized-content js-localized-content-no-show" data-hide-for="ko-kr th-th zh-tw">
<div class="col col-use-cases-heading">
<h3 class="strong">We’re all about the customer</h3>
</div>
</div>
<div class="tabs-module js-tabs-module horizontal">
<div class="tabs-container js-tabs-container js-localized-content js-localized-content-no-show" data-hide-for="ko-kr th-th zh-tw">
<div class="supertitle" data-tab="1" data-personalize="use-case-tab-1">Support</div>
<div class="supertitle" data-tab="2" data-personalize="use-case-tab-2">Sales</div>
</div>
<div class="content-container">
<div class="row row-use-case row-use-case-1" data-tab-content="1" data-personalize="use-case-1">
<div class="col col-small-6 col-use-case-images">
<div class="images-container">
<div class="photo-wrap">
<img class="lazyload no-src js-localized-image photo" data-personalize="use-case-1-photo" src="#" alt="Photo: Customer support">
<div class="mask"></div>
</div>
<img class="lazyload no-src js-localized-image screenshot" data-personalize="use-case-1-screenshot" src="#" alt="Product screenshot: Support">
</div>
</div>
<div class="col col-small-5 col-use-case-copy">
<div>
<h3 class="supertitle" data-personalize="use-case-1-supertitle-1">Support</h3>
<h2 class="strong" data-personalize="use-case-1-heading">Keep customers happy</h2>
<p data-personalize="use-case-1-copy">Your customers want to talk to you—make it easy for them. Our support products allow customer conversations to flow seamlessly across all channels, which means more productive agents and more satisfied customers.</p>
<div class="use-case-ctas">
<a class="anchor-styled anchor" href="#" data-personalize="use-case-1-learn-more-link-1" target="" rel="">Learn more</a> <div class="SL_swap" id="homepage-wistia-video-pb5j7gkmaf" data-personalize="use-case-1-video-link">
<!--<div class="wistia_embed wistia_async_pb5j7gkmaf popover=true popoverContent=link popoverSize=960x540">--><a class="anchor-styled anchor-video anchor anchor-small" tabindex="0">Watch video</a></div> </div>
</div>
</div>
<div class="internal-help-desk">
<h3 class="supertitle" data-personalize="use-case-1-supertitle-2">Internal help desk</h3>
<p data-personalize="use-case-1-copy">Employees are customers, too—with our internal helpdesk, you can create consumer-like experiences for your employees that are easy, reliable, and more productive.</p>
<div class="use-case-ctas">
<a class="anchor-styled anchor" href="#" data-personalize="use-case-1-learn-more-link-2" target="" rel="">Learn more</a> </div>
</div>
</div>
</div>
<div class="row row-use-case row-use-case-2 js-sales-content js-localized-content js-localized-content-no-show" data-tab-content="2" data-personalize="use-case-2" data-hide-for="ko-kr th-th zh-tw">
<div class="col col-small-6 col-use-case-images">
<div class="images-container">
<div class="photo-wrap">
<img class="lazyload no-src js-localized-image photo" data-personalize="use-case-2-photo" src="#" alt="Photo: Sales">
<div class="mask"></div>
</div>
<img class="lazyload no-src js-localized-image screenshot" data-personalize="use-case-2-screenshot" src="#" alt="Product screenshot: Sell">
</div>
</div>
<div class="col col-small-5 col-use-case-copy">
<h3 class="supertitle" data-personalize="use-case-2-supertitle">Sales</h3>
<h2 class="strong" data-personalize="use-case-2-heading">Win customers over</h2>
<p data-personalize="use-case-2-copy">Turn conversations into conversions. Our sales management tools are designed to enhance productivity, processes, and pipeline visibility for sales teams.</p>
<div class="use-case-ctas">
<a class="anchor-styled anchor" href="#" data-personalize="use-case-2-learn-more-link" target="" rel="">Learn more</a> </div>
<div class="use-case-quote SL_swap" id="homepage-quote-2" data-personalize="use-case-2-quote">
<img class="quote-logo no-src lazyload" alt="Logo: Staples Canada">
<p class="quote-copy" data-personalize="use-case-2-quote-copy">“By giving our sales and support teams everything they need in one platform, they are able to effectively and efficiently collaborate and improve the customer experience.”</p>
<p class="quote-author" data-personalize="use-case-2-quote-author">- Simon Rodrigue, Senior Vice President and Chief Digital Officer</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section-products">
<div class="container grid padded">
<div class="row">
<div class="col col-medium-6 col-product-links">
<div class="row row-product-links">
<a class="product-link js-product-link" data-product="support" href="#">
<div class="col col-2 col-product-icon">
<span class="product-icon product-icon-support-white"></span>
</div>
<div class="col col-4 col-product-desc">
<h4>support</h4>
<p><span class="product-faux-link">Integrated customer support</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</div>
</a>
<a class="product-link js-product-link" data-product="sell" href="#">
<div class="col col-4 col-product-icon">
<span class="product-icon product-icon-sell-white"></span>
</div>
<div class="col col-8 col-product-desc">
<h4>sell</h4>
<p><span class="product-faux-link">Sales CRM</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</div>
</a>
<a class="product-link js-product-link" data-product="guide" href="#">
<div class="col col-2 col-product-icon">
<span class="product-icon product-icon-guide-white"></span>
</div>
<div class="col col-4 col-product-desc">
<h4>guide</h4>
<p><span class="product-faux-link">Knowledge base and smart self-service</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</div>
</a>
<a class="product-link js-product-link" data-product="explore" href="#">
<div class="col col-4 col-product-icon">
<span class="product-icon product-icon-explore-white"></span>
</div>
<div class="col col-8 col-product-desc">
<h4>explore</h4>
<p><span class="product-faux-link">Analytics and reporting</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</div>
</a>
<a class="product-link js-product-link" data-product="chat" href="#">
<div class="col col-4 col-product-icon">
<span class="product-icon product-icon-chat-white"></span>
</div>
<div class="col col-8 col-product-desc">
<h4>chat</h4>
<p><span class="product-faux-link">Live chat and messaging</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</div>
</a>
<a class="product-link js-product-link" data-product="gather" href="#">
<div class="col col-4 col-product-icon">
<span class="product-icon product-icon-gather-white"></span>
</div>
<div class="col col-8 col-product-desc">
<h4>gather</h4>
<p><span class="product-faux-link">Community forum</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</div>
</a>
<a class="product-link js-product-link" data-product="talk" href="#">
<div class="col col-4 col-product-icon">
<span class="product-icon product-icon-talk-white"></span>
</div>
<div class="col col-8 col-product-desc">
<h4>talk</h4>
<p><span class="product-faux-link">Call center software</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</div>
</a>
<a class="product-link js-product-link" data-product="connect" href="#">
<div class="col col-4 col-product-icon">
<span class="product-icon product-icon-connect-white"></span>
</div>
<div class="col col-8 col-product-desc">
<h4>connect</h4>
<p><span class="product-faux-link">Proactive campaigns</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</div>
</a>
</div>
<div class="row row-product-links">
<div class="col col-xsmall-6 col-product-additional">
<a class="product-link product-link-alt" href="#">
<h4>The Suite</h4>
<p><span class="product-faux-link">Four products, one package: Support, Guide, Chat, and Talk</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</a>
</div>
<div class="col col-xsmall-6 col-product-additional">
<a class="product-link product-link-alt" href="#">
<h4>Duet</h4>
<p><span class="product-faux-link">Combine the power of Sell and Support in a single super seat</span>&nbsp;<span class="icon-chevron-right-fill"></span></p>
</a>
</div>
</div>
</div>
<div class="col col-medium-6 col-product-examples">
<h2 class="strong">Something for everyone</h2>
<p class="desc">The customer journey differs for everybody—whether you use them together or on their own, our products are flexible enough to pave the path that’s best for your business.</p>
<div class="product-screenshot-container">
<div class="product-screenshot js-product-screenshot" data-product="support">
<img class="lazyload no-src js-localized-image" src="#" alt="Product screenshot: Zendesk Support">
</div>
<div class="product-screenshot js-product-screenshot" data-product="guide">
<img class="lazyload no-src js-localized-image" src="#" alt="Product screenshot: Zendesk Guide">
 </div>
<div class="product-screenshot js-product-screenshot" data-product="chat">
<img class="lazyload no-src js-localized-image" src="#" alt="Product screenshot: Zendesk Chat">
</div>
<div class="product-screenshot js-product-screenshot" data-product="talk">
<img class="lazyload no-src js-localized-image" src="#" alt="Product screenshot: Zendesk Talk">
</div>
<div class="product-screenshot js-product-screenshot" data-product="sell">
<img class="lazyload no-src js-localized-image" src="#" alt="Product screenshot: Zendesk Sell">
</div>
<div class="product-screenshot js-product-screenshot" data-product="explore">
<img class="lazyload no-src js-localized-image" src="#" alt="Product screenshot: Zendesk Explore">
</div>
<div class="product-screenshot js-product-screenshot" data-product="gather">
<img class="lazyload no-src js-localized-image" src="#" alt="Product screenshot: Zendesk Gather">
</div>
<div class="product-screenshot js-product-screenshot" data-product="connect">
<img class="lazyload no-src js-localized-image" src="#" alt="Product screenshot: Zendesk Connect">
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section-platform js-section-platform">
<div class="platform-video-container">
<video class="lazyload platform-video js-platform-video" autoplay muted>
<source src="#" type="video/webm" />
<source src="#" type="video/mp4" />
</video>
</div>
<div class="container grid">
<div class="row row-platform-copy">
<div class="col col-small-8 col-platform-copy">
<div class="platform-logo">
<span class="product-icon-sunshine shadow ss2"></span>
<h3 class="wordmark-sunshine">sunshine</h3>
</div>
<h2 class="strong" data-personalize="platform-heading">Build with, on, and beyond</h2>
<p data-personalize="platform-copy">Build your own solution with Zendesk Sunshine, the open and flexible CRM platform. Sunshine is built on AWS and lets you seamlessly connect and understand all your customer data—wherever it lives. It’s fast, powerful, and gives you the freedom to build whatever you want.</p>
<div class="platform-ctas">
<a class="anchor-styled anchor" href="#" data-personalize="platform-learn-more-link" target="" rel="">Learn more</a> <div class="SL_swap" id="homepage-wistia-video-d9iz4w3lda" data-personalize="platform-video-link">
<div class="wistia_embed wistia_async_d9iz4w3lda popover=true popoverContent=link popoverSize=960x540"><a class="anchor-styled anchor-video anchor anchor-small" tabindex="0">Watch video</a></div> </div>
</div>
</div>
</div>
</div>
</section>
<section class="section-customer-experience">
<div class="container grid padded">
<div class="row">
<div class="col col-small-4 col-photos">
<div class="photo-container">
<div class="photo photo-1">
<div class="photo-wrap">
<img class="lazyload no-src js-localized-image" src="#3" alt="Photo: Customer Experience">
<div class="mask"></div>
</div>
</div>
<div class="photo photo-2">
<div class="photo-wrap">
<img class="lazyload no-src js-localized-image" data-personalize="customer-experience-photo-2" src="#" alt="Photo: Customer Experience">
<div class="mask"></div>
</div>
</div>
<div class="photo photo-3">
<div class="photo-wrap">
<img class="lazyload no-src js-localized-image" data-personalize="customer-experience-photo-3" src="#" alt="Photo: Customer Experience">
<div class="mask"></div>
</div>
</div>
</div>
</div>
<div class="col col-small-6 col-medium-8 col-cta">
<h2 class="h1 strong cta-heading" data-personalize="customer-experience-heading">The best customer experiences are built with Zendesk</h2>
<a href="#" data-personalize="customer-experience-cta" class="button button-large button-primary-default" target="" rel="">Free trial</a> </div>
</div>
</div>
</section>
<section class="section-logos">
<div class="container grid padded centered">
<div class="row">
<div class="col">
<p class="supertitle" data-personalize="customer-logos-supertitle">Learn from the best—our customers</p>
</div>
</div>
<div class="logos-container SL_swap" id="homepage-logos" data-personalize="customer-logos">
<div class="row">
<div class="col col-6 col-small-3"><img class="no-src lazyload logo" alt="Logo: Black &amp; Decker"></div>
<div class="col col-6 col-small-3"><img class="no-src lazyload logo" alt="Logo: Ingersoll Rand"></div>
<div class="col col-6 col-small-3"><img class="no-src lazyload logo" alt="Logo: Venmo"></div>
<div class="col col-6 col-small-3"><img class="no-src lazyload logo" alt="Logo: Slack"></div>
</div>
<div class="row">
<div class="col col-6 col-small-3"><img class="no-src lazyload logo" alt="Logo: Tesco"></div>
<div class="col col-6 col-small-3"><img class="no-src lazyload logo" alt="Logo: OpenTable"></div>
<div class="col col-6 col-small-3"><img class="no-src lazyload logo" alt="Logo: Uber"></div>
<div class="col col-6 col-small-3"><img class="no-src lazyload logo" alt="Logo: Shopify"></div>
</div>
</div>
<div class="row">
<div class="col col-cta">
<a class="anchor-styled anchor" href="#" target="" rel="">See all customer stories</a> </div>
</div>
</div>
</section>
<section class="section-articles">
<div class="container grid padded">
<div class="row SL_swap" id="homepage-article-list">
<div class="col col-medium-4 col-articles-heading">
<h3 class="strong" data-personalize="article-list-heading">There’s more where that came from</h3>
</div>
<div class="col col-xsmall-6 col-medium-4 col-articles-1" data-personalize="articles-list">
<ul class="article-list">
<li class="article-list-item"><a class="anchor-styled anchor anchor-large" href="#" target="" rel="">16 critical customer service skills you should master</a></li>
<li class="article-list-item"><a class="anchor-styled anchor anchor-large" href="#" target="" rel="">What is good customer service?</a></li>
<li class="article-list-item"><a class="anchor-styled anchor anchor-large" href="#" target="" rel="">3 customer service metrics that matter</a></li>
<li class="article-list-item"><a class="anchor-styled anchor anchor-large" href="#" target="" rel="">Most important customer service objectives</a></li>
</ul>
</div>
<div class="col col-xsmall-6 col-medium-4 col-articles-2">
<ul class="article-list">
<li class="article-list-item"><a class="anchor-styled anchor anchor-large" href="#" target="" rel="">Understanding machine learning vs deep learning</a></li>
<li class="article-list-item"><a class="anchor-styled anchor anchor-large" href="#" target="" rel="">Types of customer service you should know about</a></li>
<li class="article-list-item"><a class="anchor-styled anchor anchor-large" href="#" target="" rel="">What is customer retention?</a></li>
<li class="article-list-item"><a class="anchor-styled anchor anchor-large" href="#" target="" rel="">3 best knowledge management examples</a></li>
</ul>
</div>
</div>
</div>
</section>
</article>
<div id="proactive-offer-container"></div>
<div class="cookie-notification" id="js-cookie-notification">
<p>By continuing to use this website, you consent to the use of cookies in accordance with our <a class="cookie-policy-link js-cookie-policy-link" href="#">Cookie&nbsp;Policy</a>.</p>
<a class="close-notice js-disable-notification" href="#"><span class="close-icon"></span></a>
</div>
<footer class="global-footer">
<nav class="container">
<div class="grid five-column primary-nav">
<div class="row">
<div class="col col-1 SL_swap" id="smartling-nav-footer-product">
<h6><a href="#">Our Products</a></h6>
<ul class="no-list-style">
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">The Zendesk Suite</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Support</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Guide</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Chat</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Talk</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Sell</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Explore</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Gather</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Connect</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Sunshine</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#" target="_blank">Sunshine Conversations</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Integrations &amp; Apps</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Product Updates</a></li>
</ul>
</div>
<div class="col col-1 SL_swap" id="smartling-nav-footer-features">
<h6><a href="/">Top Features</a></h6>
<ul class="no-list-style">
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Ticketing System</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Knowledge Base</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Community Forums</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Help Desk Software</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children SL_hide i18n-hide"><a href="#">IT Help Desk</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Security</a></li>
</ul>
</div>
<div class="col col-1 SL_swap" id="smartling-nav-footer-resources">
<h6><a href="#">Resources</a></h6>
<ul class="no-list-style">
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Product Support</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Request a demo</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Library</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children SL_norewrite"><a href="#">Zendesk Blog</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children i18n-hide"><a href="#">Live webinars</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Training</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children SL_hide i18n-hide"><a href="#">API &#038; Developers</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Services &#038; Partners</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children SL_hide i18n-hide"><a href="#">For Retailers</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Relate by Zendesk</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Webinars</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Customer Stories</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Services</a></li>
</ul>
</div>
<div class="col col-1 SL_swap" id="smartling-nav-footer-company">
<h6><a href="#">Company</a></h6>
<ul class="no-list-style">
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">About us</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Press</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Investors</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Events</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Careers</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Diversity & Inclusion</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children SL_hide i18n-hide"><a href="#">Neighbor Foundation</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Contact us</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Sitemap</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">System Status</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Product Help</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Legal</a></li>
</ul>
</div>
<div class="col col-1 SL_swap" id="smartling-nav-footer-favorites">
<h6><a href="#">Favorite Things</a></h6>
<ul class="no-list-style">
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Zendesk for Enterprise</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Zendesk for Startups</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children SL_hide i18n-hide"><a href="#">Sh*t Agents Say</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children SL_hide i18n-hide"><a href="#">Zoe Calls Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Zendesk Benchmark</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Zendesk for Small Business</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children "><a href="#">Gartner CRM Magic Quadrant</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children SL_hide i18n-hide"><a href="#">Hiring Great Support Teams</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom no_children"><a href="#">Customer Experience Trends</a></li>
</ul>
</div>
</div>
</div>
<div class="grid secondary-nav">
<div class="row">
<div class="col col-small-6 col-medium-7 newsletter-section">
<div class="flex-container">
<div class="newsletter-text">
<h6>Enter the Fold</h6>
<p>Subscribe to our newsletter.</p>
</div>
<div id="newsletter-form" class="newsletter-sub">
<form id="newsletter" class="brand-kit-form js-newsletter-form-footer" method="post">
<div class="field-container js-field-container">

<div class="primary-fields">
<div class="field-wrap js-field-wrap">
<div class="email-field">
<input type="email" class="email required js-field js-footer-newsletter-email" id="owner[email]" name="owner[email]" autocomplete="email" placeholder="What&apos;s your email?" tabindex="0" /> 
<button class="form-submit-btn js-submit-btn" type="submit"><span class="text">Subscribe</span></button>

</div>
<div class="field-error js-field-error">Please enter a valid email address</div> </div>
</div>

<div class="field-wrap opt-in-field js-opt-in-field">
<div class="field-wrap js-field-wrap opt-in-checkbox js-opt-in-checkbox" data-force="">
<label class="checkbox-container">
<input type="checkbox" class="js-opt-in-checkbox js-field" id="optinToMarketing" name="optinToMarketing" checked value="TRUE">
<span class="custom-check"></span>
Please also send me occasional emails about Zendesk products and services. (You can unsubscribe at any time.) </label>
</div>
<div class="field-wrap js-field-wrap opt-in-radio js-opt-in-radio">
<div class="radio-button-container js-radio-button-container">
<span class="radio-button-copy">Please also send me occasional emails about Zendesk products and services. (You can unsubscribe at any time.)</span>
<div class="radio-button-fields js-radio-button-fields">
<label class="radio-label" for="optinToMarketing-TRUE">Yes
<input class="js-opt-in-radio js-field required" type="radio" name="optinToMarketing" id="optinToMarketing-TRUE" value="TRUE">
<span class="custom-radio"></span>
</label>
<label class="radio-label" for="optinToMarketing-FALSE">No
<input class="js-opt-in-radio js-field required" type="radio" name="optinToMarketing" id="optinToMarketing-FALSE" value="FALSE">
<span class="custom-radio"></span>
</label>
</div>
<div class="field-error js-field-error">Please select an option</div> </div>
</div>
</div>

<div style="display: none;">
<input type="hidden" name="Web_Offer_Name__c" id="Web_Offer_Name__c" value="Home" /><input type="hidden" name="GA_Medium" id="GA_Medium" value="" /><input type="hidden" name="GA_Source" id="GA_Source" value="" /><input type="hidden" name="Opti_ID" id="Opti_ID" value="" /><input type="hidden" name="Opti_Variation_ID" id="Opti_Variation_ID" value="" /><input type="hidden" name="trial_extras[Opti_ID]" id="trial_extras[Opti_ID]" value="" /><input type="hidden" name="trial_extras[Opti_Variation_ID]" id="trial_extras[Opti_Variation_ID]" value="" /><input type="hidden" name="trial_extras[SegmentAnonID]" id="trial_extras[SegmentAnonID]" value="" /><input type="hidden" name="trial_extras[Inferred_City]" id="trial_extras[Inferred_City]" value="" /><input type="hidden" name="trial_extras[Inferred_State]" id="trial_extras[Inferred_State]" value="" /><input type="hidden" name="trial_extras[Inferred_CCode]" id="trial_extras[Inferred_CCode]" value="" /><input type="hidden" name="trial_extras[Inferred_Country]" id="trial_extras[Inferred_Country]" value="" /><input type="hidden" name="trial_extras[Inferred_Zip]" id="trial_extras[Inferred_Zip]" value="" /><input type="hidden" name="trial_extras[Inferred_Region]" id="trial_extras[Inferred_Region]" value="" /><input type="hidden" name="trial_extras[Inferred_IP_Address]" id="trial_extras[Inferred_IP_Address]" value="" /><input type="hidden" name="address[postal]" id="address[postal]" value="" /><input type="hidden" name="rv_account" id="rv_account" value="" /><input type="hidden" name="rv_member" id="rv_member" value="" /><input type="hidden" name="location[Inferred_IP_Address]" id="location[Inferred_IP_Address]" value="" /><input type="hidden" name="trial_extras[DB_City__c]" id="trial_extras[DB_City__c]" value="" /><input type="hidden" name="trial_extras[DB_State__c]" id="trial_extras[DB_State__c]" value="" /><input type="hidden" name="trial_extras[DB_CCode__c]" id="trial_extras[DB_CCode__c]" value="" /><input type="hidden" name="trial_extras[Country__c]" id="trial_extras[Country__c]" value="" /><input type="hidden" name="trial_extras[DB_Zip__c]" id="trial_extras[DB_Zip__c]" value="" /><input type="hidden" name="trial_extras[Region__c]" id="trial_extras[Region__c]" value="" /><input type="hidden" name="trial_extras[First_Touch__c]" id="trial_extras[First_Touch__c]" /><input type="hidden" name="trial_extras[Last_Touch__c]" id="trial_extras[Last_Touch__c]" /><input type="hidden" name="timeStarted" id="timeStarted" value="" /><input type="hidden" name="timeDelta" id="timeDelta" value="" /><input type="hidden" name="timeEnded" id="timeEnded" value="" /><input type="hidden" name="industry" id="industry" value="" /><input type="hidden" name="MailingCountry" id="MailingCountry" value="" /><input type="hidden" name="owner[name]" id="owner[name]" value="" /><input type="hidden" name="last_page_before_lead" id="last_page_before_lead" value="" /><input type="hidden" name="current_page_url" id="current_page_url" value="" /> <input type="hidden" name="account[name]" id="account[name]" value="" /><input type="hidden" name="account[help_desk_size]" id="account[help_desk_size]" value="" /> <input type="hidden" name="elqCustomerGUID" id="elqCustomerGUID" value="" /><input type="hidden" name="elqCookieWrite" id="elqCookieWrite" value="0" /><input type="hidden" name="elqSiteId" id="elqSiteId" value="2136619493" /><input type="hidden" name="elqCampaignId" id="elqCampaignId" value="" /><input type="hidden" name="elqFormName" id="elqFormName" value="WebsiteNewsletterSignup_new" /> </div>
</div>
<div class="success-message js-success-message">
<span class="icon-success"></span>
<p>Welcome to the club!</p>
</div>
<div class="error-message js-error-message">
<span class="icon-error"></span>
<p>Sorry, something went wrong!</p><p>Please reload the page and try again, or you can email us directly at <a class="text-link" href="mailto:sdhamija1997@gmail.com">sdhamija1997@gmail.com</a>.</p> </div>
<div class="loading js-loading">
<div class="loading-inner">
<span class="loading-img"></span>
</div>
</div>
</form> </div>
</div>
</div>
<div class="col col-small-6 col-medium-5 social SL_swap" id="smartling-nav-footer-social">
<span itemscope itemtype="#">
<link itemprop="url" href="#">
<a itemprop="sameAs" aria-label="Twitter" href="#" class="notranslate icon-twitter"></a>
<a itemprop="sameAs" aria-label="Facebook" href="#" class="notranslate icon-facebook"></a>
<a itemprop="sameAs" aria-label="LinkedIn" href="#" class="notranslate icon-linkedin"></a>
<a itemprop="sameAs" aria-label="YouTube" href="#" class="notranslate icon-youtube" rel="publisher"></a>
<a itemprop="sameAs" aria-label="Instagram" href="#" class="notranslate icon-instagram"></a>
<a itemprop="sameAs" aria-label="Snapchat" href="#" class="notranslate icon-snapchat"></a>
</span>
</div>
</div>
</div>
<div class="legal-nav">
<a href="#">Terms of Use</a>
<a href="#">Privacy Policy</a>
<a href="#">Cookie Policy</a>
<a href="#">&copy;Krishna International 2020</a>
</div>
</nav>
</footer>
<script src="plugins.min.js"></script><script src="cookie.min.js"></script><script src="form.min.js"></script><script src="web.min.js"></script><script src="geo.min.js"></script>

<script src="script.js"></script>
<script type="text/javascript">
  webutils.globalNav.init();
  webutils.gauge();
  webutils.loadEloqua();
</script>
<script src="brandkit.min.js"></script><script src="parahome.min.js"></script><script src="tracking.min.js"></script>
<div id="ouibounce-modal" data-popup-type="demo">
<div class="dim-backg"></div>
<div class="center-modal">
<div class="x-close"></div>
<div class="modal-content">
<div class="content">
<h4>Join us for a live product demo</h4>
<p>We'll walk you through the product family and answer any questions you have about Zendesk.</p>
</div>
<a href="#" class="button button-large button-primary-default" target="" rel="">Schedule a demo</a> </div>
</div>
</div>
<script src="uibounce.min.js"></script><script src="config.min.js"></script>
</body>
</html>
