<?php 
session_start();

require 'config/db.php';

$errors = array();
$username = "";
$email = "";

if(isset($_POST['signup-btn'])) {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $passwordConf = $_POST['passwordConf'];

    if(empty($username)) {
        $errors['username'] = "Username can't be blank";
    }
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = "Invalid email address";
    }
    if(empty($email)) {
        $errors['email'] = "Email can't be blank";
    }
    if(empty($password)) {
        $errors['password'] = "Password can't be blank";
    }
    if($password !== $passwordConf) {
        $errors['password'] = "Password didn't match";
    }

    $emailQ = "SELECT * FROM users WHERE email=? LIMIT 1";
    $stmt = $conn->prepare($emailQ);
    $stmt->bind_param('s', $email);
    $stmt->execute();
    $result = $stmt->get_result();
    $userCount = $result->num_rows;
    $stmt->close();

    if($userCount > 0) {
        $errors['email'] = "Email already exists";
    }

    if(count($errors) == 0) {
        $password = password_hash($password, PASSWORD_DEFAULT);
        $token = bin2hex(random_bytes(50));
        $verified = false;

        $insertQ = "INSERT INTO users(username, email, verified, token, password) VALUES (?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($insertQ);
        $stmt->bind_param('ssbss', $username, $email, $verified, $token, $password);
        
        if($stmt->execute()) {
            $user_id = $conn->insert_id;
            $_SESSION['id'] = $user_id;
            $_SESSION['username'] = $username;
            $_SESSION['email'] = $email;
            $_SESSION['verified'] = $verified;
            $_SESSION['message'] = "Please check your email";
            $_SESSION['alert-class'] = "alert-success";
            header('location: login.php');
            exit();
        }
        else {
            $errors['db_error'] = "Fatal Error: Failed to Register";
        }
    }

}

if(isset($_POST['login-btn'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if(empty($username)) {
        $errors['username'] = "Username can't be empty";
    }
    if(empty($password)) {
        $errors['password'] = "Password can't be empty";
    }

    if(count($errors) === 0) {
        $usernameQ = "SELECT * FROM users WHERE email=? OR username=? LIMIT 1";
        $stmt = $conn->prepare($usernameQ);
        $stmt->bind_param('ss', $username, $username);
        $stmt->execute();
        $result = $stmt->get_result();
        $user = $result->fetch_assoc();

        if(password_verify($password, $user['password'])) {
            //Successfully Login
            $_SESSION['id'] = $user['id'];
            $_SESSION['username'] = $user['username'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['verified'] = $user['verified'];

            $_SESSION['message'] = "You're logged in";
            $_SESSION['alert-class'] = "alert-success";
            header('location:welcome.php');
            exit();
        }
        else {
            $errors['login_fail'] = "Invalid username or password";
        }
    }
}

if(isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['id']);
    unset($_SESSION['username']);
    unset($_SESSION['email']);
    unset($_SESSION['verified']);
    header('location:login.php');
    exit();
}
?>