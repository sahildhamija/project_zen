<?php 
require_once 'controllers/authController.php'; 
if(!isset($_SESSION['id'])) {
    header('location: index.html');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="register.css">
    <title>Confirm Registration | Zendesk</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4 form-div login">
                <?php if(isset($_SESSION['message'])): ?>
                <div class="alert <?php echo $_SESSION['alert-class']; ?>">
                    <?php 
                        echo $_SESSION['message'];
                        unset($_SESSION['message']);
                        unset($_SESSION['alert-class']); 
                    ?>
                </div>
                <?php endif; ?>
                <h3>Welcome, <?php echo $_SESSION['username']; ?></h3>
                <?php if(!$_SESSION['verified']): ?>
                <div class="alert alert-warning">
                    We've send you a verification link on <strong><?php echo $_SESSION['email']; ?></strong>
                    Please signin to your account and click on that link to verify your account.
                </div>
                <?php endif; ?>
                <a href="#" class="">Resend activation link</a>
                <a href="confirm.php?logout=1" class="logout">Logout</a>
                <?php if($_SESSION['verified']): ?>
                <button class="btn btn-block btn-lg btn-primary">Confirm Verification</button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</body>
</html>